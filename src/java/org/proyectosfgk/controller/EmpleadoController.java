/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.controller;

import javax.servlet.http.HttpServletRequest;
import org.proyectosfgk.dao.DepartamentoDAO;
import org.proyectosfgk.dao.EmpleadoDAO;
import org.proyectosfgk.dao.RolDAO;
import org.proyectosfgk.entidad.Empleado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author rodolfo.molinafgkss
 */
@Controller
@RequestMapping("/empleados")
public class EmpleadoController {

    @Autowired
    EmpleadoDAO empleadoDao;
    
    @Autowired
    DepartamentoDAO depDao;
    
    @Autowired
    RolDAO rolDao;
        
//    @Autowired
//    private BCryptPasswordEncoder bcrypto;
//    
    /**
     * Controlador par acceder a la página
     * crear empleado
     * @return página crearEmpleado
     */
    @RequestMapping(value = "/crearEmpleado", method = RequestMethod.GET)
    public ModelAndView crearEmpleadoPage() {
        ModelAndView modelAndView = new ModelAndView("crearEmpleado");
        modelAndView.addObject("empleado", new Empleado());
        modelAndView.addObject("listaDepartamentos", depDao.buscarDepartamento());
        modelAndView.addObject("listaRoles", rolDao.buscarR());
        return modelAndView;
    }
    
    
    /**
     * ---CREATE---
     * Controlador para crear empleado
     * @param empleado
     * @return 
     */
    
    @RequestMapping(value = "/crearEmpleado", method = RequestMethod.POST)
    public String create(Empleado empleado){
        empleado.setCodEmpleado(new BCryptPasswordEncoder().encode(empleado.getCodEmpleado()));
        empleado.setDisponible(true);
        empleadoDao.create(empleado);
        return "redirect:/empleados/verEmpleados.htm";
    }
    
    
    /**
     * ---READ--- 
     * Controlador para mostrar lista 
     * de empleados creados
     * @return página verEmpleados
     */
    @RequestMapping(value = "/verEmpleados", method = RequestMethod.GET)
    public ModelAndView findAll() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("verEmpleados");        
        modelAndView.addObject("listaEmpleados", empleadoDao.findAll());
        return modelAndView;
    }
    
    /**
     * Método que captura datos que se van a editar
     * @param request
     * @return págin editar con campos llenos
     */
    
    @RequestMapping(value = "/modificarEmpleado", method = RequestMethod.GET)
    public ModelAndView editar(HttpServletRequest request){
        ModelAndView mav = new ModelAndView("modificarEmpleado");
        mav.addObject("empleado", empleadoDao.findById(Integer.parseInt(request.getParameter("id"))));
        mav.addObject("listaDepartamentos", depDao.buscarDepartamento());
        mav.addObject("listaRoles", rolDao.buscarR());
        return mav;
    }
    
    /**
     * Método para guardar cambios luego de editar
     * @param empleado
     * @param request
     * @return página con lista de empleados
     */
    
    @RequestMapping(value = "/modificarEmpleado", method = RequestMethod.POST)
    public String editar(Empleado empleado, HttpServletRequest request){
        empleado.setCodEmpleado(new BCryptPasswordEncoder().encode(empleado.getCodEmpleado()));
        Empleado emp = empleadoDao.findById(empleado.getId());
        if(request.getParameter("codEmpleado").isEmpty()){
            empleado.setCodEmpleado(emp.getCodEmpleado());            
        }
        System.out.println(empleado.getCodEmpleado());
        empleadoDao.update(empleado);
        return "redirect:/empleados/verEmpleados.htm";
    }
    
    /**
     * Método para habilitar usuario
     * @param request
     * @return 
     */
    
    @RequestMapping(value = "/habilitar", method = RequestMethod.GET)
    public String habilitarEmpleado(HttpServletRequest request){
        empleadoDao.habilitarEmpleado("habilitar", empleadoDao.findById(Integer.parseInt(request.getParameter("id"))));
        return "redirect:/empleados/verEmpleados.htm";
    }
    
    /**
     * Método para inhabilitar usuario
     * @param request
     * @return 
     */
    
    @RequestMapping(value ="/inhabilitar", method = RequestMethod.GET)
    public String inhabilitarEmpleado(HttpServletRequest request){
        empleadoDao.inhabilitarEmpleado("inhabilitar", empleadoDao.findById(Integer.parseInt(request.getParameter("id"))));
        return "redirect:/empleado/verEmpleados.htm";
    }

}
