package org.proyectosfgk.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.proyectosfgk.dao.EmpleadoDAO;
import org.proyectosfgk.dao.EmpleadoPermisoDAO;
import org.proyectosfgk.dao.ProyectoDAO;
import org.proyectosfgk.entidad.Empleado;
import org.proyectosfgk.entidad.EmpleadoPermiso;
import org.proyectosfgk.entidad.EmpleadoProyecto;
import org.proyectosfgk.entidad.Proyecto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/proyectos")
public class ProyectoController {

    @Autowired
    EmpleadoDAO dAO;

    @Autowired
    EmpleadoPermisoDAO dAO1;

    @Autowired
    ProyectoDAO dao;

    /**
     * Método para ver página createProyecto
     *
     * @return página createProyecto
     */
    @RequestMapping(value = "/crearProyecto.htm", method = RequestMethod.GET)
    public ModelAndView createProjectPage() {
        ModelAndView mav = new ModelAndView("/crearProyecto");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        mav.addObject("user", dAO.getByUserName(authentication.getName()));
        List<EmpleadoPermiso> lista = dAO1.getByEmployed(dAO.getByUserName(authentication.getName()));
        for (EmpleadoPermiso e : lista) {
            mav.addObject(e.getPermisoId().getNombre(), e);
        }

        Map< String, String> tiposP = new HashMap<>();
        Map< Integer, String> lideres = new HashMap<>();

        tiposP.put("Interno", "Interno");
        tiposP.put("Externo", "Externo");

        for (Empleado emp : dAO.buscarDispo()) {
            lideres.put(emp.getId(), emp.getNombre() + " " + emp.getApellido());
        }
        mav.addObject("proyecto", new Proyecto());
        mav.addObject("lideresMap", lideres);
        mav.addObject("tiposMap", tiposP);

        return mav;
    }

    /**
     * Método para crear un proyecto
     *
     * @param proyecto
     * @param fechaI
     * @param fechaF
     * @return
     */
    @RequestMapping(value = "/crearProyecto.htm", method = RequestMethod.POST)
    public ModelAndView create(@ModelAttribute("proyecto") Proyecto proyecto, @RequestParam("fechaI") String fechaI, @RequestParam("fechaF") String fechaF) {
        ModelAndView mav = new ModelAndView();
        SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
        try {
            //Date dateI = ;
            proyecto.setFechaInicio(formateador.parse(fechaI));
        } catch (ParseException ex) {
            Logger.getLogger(ProyectoController.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            proyecto.setFechaFin(formateador.parse(fechaF));
        } catch (ParseException ex) {
            Logger.getLogger(ProyectoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        Empleado emp = dAO.findById(proyecto.getLiderProyecto().getId());
        emp.setDisponible(false);

        proyecto.setLiderProyecto(emp);
        proyecto.setRequerimientos(proyecto.getArchivo().getBytes());
        proyecto.setNombreArchivo(proyecto.getArchivo().getOriginalFilename());
        proyecto.setFormato(proyecto.getArchivo().getContentType());
        if (proyecto.getNombreArchivo().toLowerCase().contains(".doc") || proyecto.getNombreArchivo().toLowerCase().contains(".docx")
                || proyecto.getNombreArchivo().toLowerCase().contains(".txt") || proyecto.getNombreArchivo().toLowerCase().contains(".pdf")
                || proyecto.getNombreArchivo().equals("")) {
            dao.crear(proyecto);
            dAO.update(emp);
            mav.setViewName("redirect:/proyectos/verProyectos.htm");
        } else {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            mav.addObject("user", dAO.getByUserName(authentication.getName()));
            List<EmpleadoPermiso> lista = dAO1.getByEmployed(dAO.getByUserName(authentication.getName()));
            for (EmpleadoPermiso e : lista) {
                mav.addObject(e.getPermisoId().getNombre(), e);
            }

            Map< String, String> tiposP = new HashMap<>();
            Map< Integer, String> lideres = new HashMap<>();

            tiposP.put("Interno", "Interno");
            tiposP.put("Externo", "Externo");

            for (Empleado emple : dAO.buscarDispo()) {
                lideres.put(emple.getId(), emple.getNombre() + " " + emple.getApellido());
            }
            mav.addObject("proyecto", new Proyecto());
            mav.addObject("lideresMap", lideres);
            mav.addObject("tiposMap", tiposP);
            mav.addObject("m", "error");
            mav.setViewName("crearProyecto");
        }
        return mav;
    }

    /**
     * Metodo para realizar la descarga gel archivo de la base
     *
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "/download.htm", method = RequestMethod.GET)
    public void descarga(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Proyecto proyecto = dao.findById(Integer.parseInt(request.getParameter("id")));
        response.setContentType(proyecto.getFormato());
        response.setHeader("Content-Disposition", "attachment; filename= " + proyecto.getNombreArchivo() + "");

        ServletOutputStream out = response.getOutputStream();
        out.write(proyecto.getRequerimientos());
        out.flush();
        out.close();
    }

    /**
     * Metodo para capturar los datos que se podran editar
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/modificarProyecto", method = RequestMethod.GET)
    public String getProyecto(Model model, HttpServletRequest request) {
        model.addAttribute("proyecto", dao.findById(Integer.parseInt(request.getParameter("id"))));

        return "modificarProyecto";
    }

    /**
     * Metodo para guardar los cambios de las modificaciones
     *
     * @param request
     * @param proyecto
     * @param fechaFin
     * @return
     */
    @RequestMapping(value = "/modificarProyecto", method = RequestMethod.POST)
    public ModelAndView actualizacion(HttpServletRequest request, @ModelAttribute("proyecto") Proyecto proyecto, @RequestParam("fechaF") String fechaFin) {
        ModelAndView mav = new ModelAndView();
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
        if (proyecto.getArchivo().getOriginalFilename().equals("")) {
            proyecto.setRequerimientos(dao.findById(proyecto.getId()).getRequerimientos());
            proyecto.setNombreArchivo(dao.findById(proyecto.getId()).getNombreArchivo());
            proyecto.setFormato(dao.findById(proyecto.getId()).getFormato());
        } else {
            proyecto.setRequerimientos(proyecto.getArchivo().getBytes());
            proyecto.setNombreArchivo(proyecto.getArchivo().getOriginalFilename());
            proyecto.setFormato(proyecto.getArchivo().getContentType());
        }
        try {
            Date dateF = f.parse(fechaFin);
            proyecto.setFechaFin(dateF);
        } catch (ParseException ex) {
            Logger.getLogger(ProyectoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        proyecto.setFechaInicio(dao.findById(proyecto.getId()).getFechaInicio());
        proyecto.setEstado(dao.findById(proyecto.getId()).getEstado());
        proyecto.setLiderProyecto(dao.findById(proyecto.getId()).getLiderProyecto());
        if (proyecto.getNombreArchivo().toLowerCase().contains(".doc") || proyecto.getNombreArchivo().toLowerCase().contains(".docx")
                || proyecto.getNombreArchivo().toLowerCase().contains(".txt") || proyecto.getNombreArchivo().toLowerCase().contains(".pdf")) {
            dao.actualizar(proyecto);
            mav.setViewName("redirect:/proyectos/verProyectos.htm");
        }else{
            mav.addObject("proyecto", dao.findById(proyecto.getId()));
            mav.addObject("message","error");
            mav.setViewName("modificarProyecto");
        }
        

        return mav;

    }

    /**
     * Metodo para cambiar el estado de un proyecto y pasar a disponibles a
     * todos los empleados que trabajaban en el proyecto
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/finalizarProyectos.htm", method = RequestMethod.GET)
    public String finalizar(HttpServletRequest request) {
        Proyecto proyecto = dao.findById(Integer.parseInt(request.getParameter("id")));
        proyecto.setEstado("Finalizado");
        dao.actualizar(proyecto);

        Empleado emple = proyecto.getLiderProyecto();
        emple.setDisponible(true);
        dAO.update(emple);

        for (EmpleadoProyecto e : proyecto.getEmpleadoProyectoList()) {
            e.getEmpleadoId().setDisponible(true);
            dAO.update(e.getEmpleadoId());
        }

        return "redirect:/proyectos/verProyectos.htm";
    }

    /**
     * Método para ver página viewProyecto
     *
     * @return página viewProyecto
     */
    @RequestMapping(value = "/verProyectos.htm", method = RequestMethod.GET)
    public ModelAndView viewProjectPage() {
        ModelAndView mav = new ModelAndView("/verProyectos");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        mav.addObject("proyectos", dao.findAll());
        List<EmpleadoPermiso> lista = dAO1.getByEmployed(dAO.getByUserName(authentication.getName()));
        for (EmpleadoPermiso e : lista) {
            mav.addObject(e.getPermisoId().getNombre(), e);
        }
        mav.addObject("proyectos", dao.findAll());
        return mav;
    }
}
