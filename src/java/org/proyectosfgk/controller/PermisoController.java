/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.controller;

import javax.servlet.http.HttpServletRequest;
import org.proyectosfgk.dao.PermisoDAO;
import org.proyectosfgk.entidad.Permiso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


/**
 *
 * @author rodolfo.molinafgkss
 */
@Controller
@RequestMapping("/permisos")
public class PermisoController {

    @Autowired
    PermisoDAO dao;

    /**
     * Método para ver página para crear permisos
     *
     * @return página createPermiso
     */
    @RequestMapping(value = "/crearPermiso", method = RequestMethod.GET)
    public ModelAndView createPermisoPage() {
        ModelAndView mav = new ModelAndView("crearPermiso");
        mav.addObject("permiso", new Permiso());
        return mav;
    }

    /**
     * Método para crear permiso
     *
     * @param permiso
     * @return
     */
    @RequestMapping(value = "/crearPermiso", method = RequestMethod.POST)
    public String create(Permiso permiso) {
        dao.create(permiso);
        return "redirect:/permisos/verPermisos.htm";
    }
    
    /**
     * Método que captura datos que se van a editar
     * @param model
     * @param request
     * @return 
     */

    @RequestMapping(value = "/modificarPermiso", method = RequestMethod.GET)
    public String getPermiso(Model model, HttpServletRequest request) {
        model.addAttribute("permiso", dao.findById(Integer.parseInt(request.getParameter("id"))));        
        return "modificarPermiso";
    }
    
    
    /**
     * Método para guardar cambios
     * @param request
     * @return 
     */

    @RequestMapping(value = "/modificarPermiso", method = RequestMethod.POST)
    public String update(HttpServletRequest request) {
        Permiso permiso = dao.findById(Integer.parseInt(request.getParameter("id")));
        permiso.setNombre(request.getParameter("nombre"));
        dao.update(permiso);
        return "redirect:/permisos/verPermisos.htm";
    }

    /**
     * Método para eliminar permisos
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/eliminarPermiso", method = RequestMethod.GET)
    public String remove(HttpServletRequest request) {
        Permiso permiso = dao.findById(Integer.parseInt(request.getParameter("id")));
        dao.remove(permiso);
        return "redirect:/permisos/verPermisos.htm";
    }

    /**
     * Método para listar permisos
     *
     * @param modelAndView
     * @return lista de permisos
     */
    @RequestMapping(value = "/verPermisos", method = RequestMethod.GET)
    public ModelAndView findAll() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("verPermisos");
        //modelAndView.setViewName("redirect:verPermisos.htm");
        modelAndView.addObject("listaPermisos", dao.findAll());
        
        return modelAndView;
    }

}
