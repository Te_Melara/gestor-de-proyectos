
package org.proyectosfgk.controller;

import javax.servlet.http.HttpServletRequest;
import org.proyectosfgk.dao.EmpleadoPermisoDAO;
import org.proyectosfgk.entidad.EmpleadoPermiso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author fernand.vasquezfgkss
 */
@Controller
@RequestMapping("/empleadoPermisos")
public class EmpleadoPermisoController {
    
    @Autowired
    EmpleadoPermisoDAO dao;
    
    /**
     * Metodo que lista los permisos de cada empleado
     * @param request recibe el id del empleado del que se listaran los permisos
     * @return devuelve la lista de permisos asignado al empleado y los que aun le pueden ser asignados
     */
    @RequestMapping(value = "/listaByEmploy",method = RequestMethod.GET)
    public ModelAndView listaByEmploy(HttpServletRequest request){
        ModelAndView mav = new ModelAndView("/PermisosEmpleados");
        mav.addObject("permiso" , new EmpleadoPermiso());
        mav.addObject("disponibles", dao.dispoList(dao.empleados(Integer.parseInt(request.getParameter("id")))));
        mav.addObject("lista", dao.getByEmployed(dao.empleados(Integer.parseInt(request.getParameter("id")))));
        mav.addObject("empleado", dao.empleados(Integer.parseInt(request.getParameter("id"))));
        return mav;
    }
    
    /**
     * Metodo que lista los permisos de cada empleado
     * @param id recibe el id del empleado del que se listaran los permisos
     * @return devuelve la lista de permisos asignado al empleado y los que aun le pueden ser asignados
     */
    public ModelAndView listaByEmploy(int id){
        ModelAndView mav = new ModelAndView("/PermisosEmpleados");
        mav.addObject("permiso" , new EmpleadoPermiso());
        mav.addObject("disponibles", dao.dispoList(dao.empleados(id)));
        mav.addObject("lista", dao.getByEmployed(dao.empleados(id)));
        mav.addObject("empleado", dao.empleados(id));
        return mav;
    }
    
    /**
     * Activa los permisos desactivados de un empleado en especifico
     * @param request recibe el id del empleado y el id del permiso a activarse
     * @return devuelve una redireccion a el metodo que lista los permisos del empleado
     */
    @RequestMapping(value = "/activar" , method = RequestMethod.GET)
    public ModelAndView activar(HttpServletRequest request){
        dao.activar("activar", dao.empleados(Integer.parseInt(request.getParameter("idEm"))), dao.permiso(Integer.parseInt(request.getParameter("idPer"))));
        return listaByEmploy(Integer.parseInt(request.getParameter("idEm")));
    }
    
    /**
     * Desactiva los permisos de un empleado en especifico
     * @param request recibe el id del empleado y el id del permiso a desactivarse
     * @return devuelve una redireccion a el metodo que lista los permisos del empleado
     */
    @RequestMapping(value = "/desactivar" , method = RequestMethod.GET)
    public ModelAndView desactivar(HttpServletRequest request){
        dao.activar("desactivar", dao.empleados(Integer.parseInt(request.getParameter("idEm"))), dao.permiso(Integer.parseInt(request.getParameter("idPer"))));
        return listaByEmploy(Integer.parseInt(request.getParameter("idEm")));
    }
    
    /**
     * Asigna permisos a los empleados
     * @param request recibe el id del empleado y el id del permiso a asignarse
     * @return devuelve una redireccion a el metodo que lista los permisos del empleado
     */
    @RequestMapping(value = "/asignar" , method = RequestMethod.POST)
    public String asignar(HttpServletRequest request,@RequestParam("idPer")String permi){
        dao.crear(dao.empleados(Integer.parseInt(request.getParameter("idEm"))), dao.permiso(Integer.parseInt(permi)));
        return "redirect:/empleadoPermisos/listaByEmploy.htm?id="+request.getParameter("idEm");
    }
    
}
