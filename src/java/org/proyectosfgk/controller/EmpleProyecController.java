
package org.proyectosfgk.controller;

import org.proyectosfgk.dao.EmpProDAO;
import org.proyectosfgk.dao.EmpleadoDAO;
import org.proyectosfgk.dao.ProyectoDAO;
import org.proyectosfgk.entidad.Empleado;
import org.proyectosfgk.entidad.EmpleadoProyecto;
import org.proyectosfgk.entidad.Proyecto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/asignacion")
public class EmpleProyecController {
    
    @Autowired
    EmpProDAO dao;
    
    @Autowired
    ProyectoDAO proDao;
    
    @Autowired
    EmpleadoDAO empDao;
    
    /**
     * Metodo para ver pagina asignacion de proyectos
     * @return pagina empleadoProyecto
     */
    
    @RequestMapping(value = "/crearAsignacion.htm", method = RequestMethod.GET)
    public ModelAndView createAsignPage(){
        ModelAndView mav = new ModelAndView ("/crearAsignacion");
        mav.addObject("empProy", new EmpleadoProyecto());
        mav.addObject("lista", empDao.buscarDispo());
        mav.addObject("listado",proDao.buscarEnProceso());
        System.out.println("Proyectos:" + proDao.buscarEnProceso().size());
        
        return mav;
    }
    
    /**
     * Método para crear la asignacion de los proyectos
     * @param empId
     * @param proId
     * @return 
     */
    
    @RequestMapping(value="/crearAsignacion.htm", method = RequestMethod.POST)
    public String create (@RequestParam("empleadoId") int empId, @RequestParam("proyectoId") int proId){
        EmpleadoProyecto empPro = new EmpleadoProyecto();
        Empleado emp = empDao.findById(empId);        
        emp.setDisponible(false);
        empDao.update(emp);
        empPro.setEmpleadoId(emp);
        Proyecto proy = proDao.findById(proId);
        empPro.setProyectoId(proy);
        
        dao.crear(empPro);       
        return "redirect:/asignacion/verAsignacion.htm";
    }
    
    /**
     * Método para ver la pagina de los proyectos asignados
     * @return
     */
    
    @RequestMapping (value = "/verAsignacion.htm", method = RequestMethod.GET)
    public ModelAndView verTodos(){
        ModelAndView modelView = new ModelAndView();
        modelView.setViewName("verAsignacion");
        modelView.addObject("listaAsig", dao.buscarTodos());
        
        return modelView;
    }
}
