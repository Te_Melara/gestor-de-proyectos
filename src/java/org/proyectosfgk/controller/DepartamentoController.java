package org.proyectosfgk.controller;

import javax.servlet.http.HttpServletRequest;
import org.proyectosfgk.dao.DepartamentoDAO;
import org.proyectosfgk.entidad.Departamento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author amand.rodriguezfgkss
 */
@Controller
@RequestMapping("/departamento")
public class DepartamentoController {
    
    @Autowired
    DepartamentoDAO dep;
    
    
    /**
     * Método para listar permisos
     *
     * @param ModelAndView
     * @return lista de permisos
     */
    @RequestMapping(value = "/verDepartamento", method = RequestMethod.GET)
    public ModelAndView mostrar(){
        ModelAndView mav = new ModelAndView();
        //mav.addObject("depa", new Departamento());
        mav.addObject("listaD", dep.buscarDepartamento());
        mav.setViewName("verDepartamentos");
        
        return mav;
    }
    
    /**
     * Método para ver página para crear departamento
     *
     * @return página crearDepartamento
     */
    @RequestMapping (value ="/crearDepartamento", method = RequestMethod.GET)
    public String ingresar (){
        
        return "crearDepartamento";
      
    }
    /**
     * Método para crear departamento
     *
     * @param departamento
     * @return
     */
    
    @RequestMapping (value = "/crearDepartamento", method = RequestMethod.POST)
    public String create(Departamento departamento) {
        dep.crear(departamento);
        return "redirect:/departamento/verDepartamento.htm";
    }
    /**
     * Método para eliminar departamento
     *
     * @param request
     * @return
     */
    @RequestMapping (value = "/eliminarDepartamento", method = RequestMethod.GET)
    public String eliminar(HttpServletRequest request){
        dep.eliminar(dep.buscarPorId(Integer.parseInt(request.getParameter("id"))));
        return "redirect:/departamento/verDepartamento.htm";
    }
     /**
     * Método que captura datos que se van a editar
     * @param model
     * @param request
     * @return 
     */
    @RequestMapping (value ="/modificarDepartamento", method = RequestMethod.GET)
    public ModelAndView editar (Model model, HttpServletRequest request){
        ModelAndView mav = new ModelAndView();
        
        mav.addObject("depa", dep.buscarPorId(Integer.parseInt(request.getParameter("id"))));
        mav.setViewName("modificarDepartamento");
        return mav;
    }
    
    @RequestMapping(value = "/modificarDepartamento", method = RequestMethod.POST)
    public String editar(Departamento d){
        dep.actualizar(d);
        return "redirect:/departamento/verDepartamento.htm";
    }
    
}
