/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import javax.persistence.PersistenceException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.proyectosfgk.dao.BitacoraProyectoDAO;
import org.proyectosfgk.dao.EmpleadoDAO;
import org.proyectosfgk.dao.EmpleadoPermisoDAO;
import org.proyectosfgk.dao.ProyectoDAO;
import org.proyectosfgk.entidad.BitacoraProyecto;
import org.proyectosfgk.entidad.EmpleadoPermiso;
import org.proyectosfgk.entidad.File;
import org.proyectosfgk.entidad.Proyecto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author fernand.vasquezfgkss
 */
@Controller
@RequestMapping("/bitacora")
public class BitacoraProyectoController {

    @Autowired
    ProyectoDAO Pdao;

    @Autowired
    BitacoraProyectoDAO dao;

    @Autowired
    EmpleadoPermisoDAO dAO1;

    @Autowired
    EmpleadoDAO dAO;

    
    /**
     * --metodo que redirige donde se crearan y mostraran las versiones de un proyecto
     * @param request 
     * @return retorna la pagina donde se mostraran las versiones
     */
    @RequestMapping(value = "/ver", method = RequestMethod.GET)
    public ModelAndView ver(HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String m = request.getParameter("m") != null ? request.getParameter("m") : "";
        ModelAndView mav = new ModelAndView();
        if(!m.equals("")){
            mav.addObject("message",m);
        }
        mav.addObject("file", new File());
        mav.addObject("v", dao.getLastVersion(dao.getProyect(Integer.parseInt(request.getParameter("id")))));
        mav.addObject("versiones", dao.getByProject(Integer.parseInt(request.getParameter("id"))));
        mav.addObject("proyecto", dao.getProyect(Integer.parseInt(request.getParameter("id"))));
        List<EmpleadoPermiso> lista = dAO1.getByEmployed(dAO.getByUserName(authentication.getName()));
        for (EmpleadoPermiso e : lista) {
            mav.addObject(e.getPermisoId().getNombre(), e);
        }
        mav.setViewName("/verBitacora");
        return mav;
    }

    /**
     * Metodo que se encarga de guardar las versiones
     * @param file objeto que se utiliza para obtener los datos multipartes del formulario para ingresarlo en el objeto de bitacora
     * @param result Verifica si no a ocurrido ningun error
     * @return retorna la lista donde se ven las versones con un mensaje si a ocurrido algun error
     * @throws IOException Capturara el error si es que sucede al momento de obtener los bytes de el archivo
     */
    @RequestMapping(value = "/guardar", method = RequestMethod.POST)
    public String Guardar(@ModelAttribute("file") File file, BindingResult result) throws IOException {
        //DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        BitacoraProyecto bt = new BitacoraProyecto();
        String m = "";
        if (!result.hasErrors()) {
            bt.setArchivo(file.getMultipartFile().getBytes());
            bt.setVersion(Double.parseDouble(file.getVersion()));
            bt.setFecha(new Date());
            bt.setProyectoId(new Proyecto(file.getIdProyect()));
            bt.setFormato(file.getMultipartFile().getContentType());
            bt.setNombre(file.getMultipartFile().getOriginalFilename());
            try {
                if(bt.getNombre().contains(".rar") || bt.getNombre().contains(".tar.gz")
                        || bt.getNombre().contains(".zip") || bt.getNombre().contains(".7z")){
                    dao.crear(bt);
                    m = "correcto";
                } else {
                    m = "error";
                }
            } catch (PersistenceException e) {
                System.out.println("Error: " + e.getMessage());
            } catch (Exception ex) {
                System.out.println("Error: " + ex.getMessage());
            } finally {

            }
        }
        return "redirect:/bitacora/ver.htm?m=" + m + "&id=" + bt.getProyectoId().getId();
    }

    /**
     * Metodo que renderiza las versiones para su respectiva descarga
     * @param request 
     * @param response
     * @throws IOException Capturara el error si es que sucede al momento de obtener los bytes de el archivo
     */
    @RequestMapping(value = "/descargar", method = RequestMethod.GET)
    public void descargar(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Integer id = request.getParameter("id") != null ? Integer.parseInt(request.getParameter("id")) : 0;
        response.setContentType(dao.getById(id).getFormato());
        response.setHeader("Content-Disposition", "attachment; filename=" + dao.getById(id).getNombre() + "");
        response.setHeader("cache-control", "no-cache");
        ServletOutputStream outs = response.getOutputStream();
        outs.write(dao.getById(id).getArchivo());
        outs.flush();
        outs.close();
    }
}
