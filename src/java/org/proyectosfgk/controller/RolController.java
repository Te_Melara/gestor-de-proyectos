package org.proyectosfgk.controller;

import javax.servlet.http.HttpServletRequest;
import org.proyectosfgk.dao.RolDAO;
import org.proyectosfgk.entidad.Rol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author amand.rodriguezfgkss
 */
@Controller
@RequestMapping("/rol")
public class RolController {
    
    @Autowired
    
    RolDAO rol;
    
    @RequestMapping (value = "/verRoles", method = RequestMethod.GET)
    public ModelAndView mostrar(){
        ModelAndView mav = new ModelAndView();
        
        mav.addObject("roles", new Rol());
        mav.addObject("listaR", rol.buscarR());
        mav.setViewName("verRoles");
        
        return mav;
        
    }
    
    /**
     * Método para ver página para crear rol
     *
     * @return página crearRol
     */
    @RequestMapping ( value = "/crearRol", method = RequestMethod.GET)
        public String ingresar(){
            
            
            return "crearRol";
        }
        
        /**
     * Método para crear rol
     *
     * @param r
     * @return
     */
    @RequestMapping(value = "/crearRol", method = RequestMethod.POST)
	public String ingresar(Rol r){
            
		rol.crear(r);

		return "redirect:/rol/verRoles.htm";
	}
      /**
     * Método para eliminar rol
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/eliminarRol", method = RequestMethod.GET)
	public String eliminar(HttpServletRequest request) {
		rol.eliminar(rol.buscarPorId(Integer.parseInt(request.getParameter("id"))));
		return "redirect:/rol/verRoles.htm";
	}
    /**
     * Método que captura datos que se van a editar
     * @param model
     * @param request
     * @return 
     */    
    @RequestMapping(value = "/modificarRol", method = RequestMethod.GET)
        public ModelAndView editar(Model model, HttpServletRequest request){
            ModelAndView mav = new ModelAndView();
            
            mav.addObject("roles", rol.buscarPorId(Integer.parseInt(request.getParameter("id"))));
            mav.setViewName("modificarRol");
	//request.getSession().setAttribute("usuario", dao);
            return mav;
        
	}

    @RequestMapping(value = "/modificarRol", method = RequestMethod.POST)
	public String editar(Rol r) {
		
		rol.actualizar(r);

		return "redirect:/rol/verRoles.htm";
	}
    
}
