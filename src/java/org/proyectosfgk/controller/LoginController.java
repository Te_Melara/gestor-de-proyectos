/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.controller;

import java.security.Principal;
import org.proyectosfgk.dao.PermisoDAO;
import org.proyectosfgk.entidad.Empleado;
import org.proyectosfgk.entidad.Permiso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author rodolfo.molinafgkss
 */
@Controller
@RequestMapping("/")
public class LoginController {

    /**
     * Método para ver página para crear permisos
     *
     * @return página createPermiso
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView createPermisoPage() {
        ModelAndView mav = new ModelAndView("login");
        mav.addObject("login", new Permiso());
        return mav;
    }
    
//    @RequestMapping(value = "loginAccess", method = RequestMethod.POST)
//    public String login(){
//        return "";
//    }
    
//    public Object returnUser(){
//        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        
//    }
    
    @RequestMapping(value = "/username", method = RequestMethod.GET)
    @ResponseBody
    public Empleado getCurrentUser(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(auth != null) {
            Object principal = auth.getPrincipal();
            if(principal instanceof Empleado){
                return (Empleado) principal;
            }
        }
        return null;
    }
    
}
