
package org.proyectosfgk.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import org.proyectosfgk.entidad.EmpleadoProyecto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EmpProDAO {
   @PersistenceContext
    private EntityManager em;
    
    
    /**
     * --CREATE--- Metodo para crear la asignacion 
     * de un proyecto a empleados
     * @param empPro
     * @return verdadero(insertado) o falso (no insertado)
     */
    
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    public Boolean crear (EmpleadoProyecto empPro){
        try{
            em.persist(empPro);
            return true;
        }catch (PersistenceException pe){
            System.err.println("Error de persistencia: " + pe.getMessage());
        }catch (Exception e){
            System.err.println("Error desconocido: " + e.getMessage());
        }
        
        return false;
    }
    
    /**
     * --READ-- Metodo para buscar todas las asignaciones 
     * de proyectos
     * 
     * @return 
     */
    
    @Transactional (rollbackFor ={PersistenceException.class, Exception.class})
    public List<EmpleadoProyecto> buscarTodos(){
        return (List<EmpleadoProyecto>) em.createNamedQuery("EmpleadoProyecto.findAll", EmpleadoProyecto.class).getResultList();
    } 
    
}
