/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.proyectosfgk.entidad.Empleado;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author rodolfo.molinafgkss
 */
@Service
public class EmpleadoDAO {

    @PersistenceContext
    private EntityManager em;

    /**
     * ---CREATE--- Método para crear empleado
     *
     * @param empleado
     * @return verdadero (insertado) o falso (no insertado)
     */
    @Transactional(rollbackFor = {PersistenceException.class, Exception.class})
    public Boolean create(Empleado empleado) {
        try {
            em.persist(empleado);
            return true;
        } catch (PersistenceException pe) {
            System.err.println("Error de persistencia: " + pe.getMessage());
        } catch (Exception ex) {
            System.err.println("Error desconocido: " + ex.getMessage());
        }
        return false;
    }

    /**
     * ---READ--- Método para buscar empleados
     *
     * @return lista de empleados
     */
    @Transactional(rollbackFor = {PersistenceException.class, Exception.class})
    public List<Empleado> findAll() {
        return em.createNamedQuery("Empleado.findAll", Empleado.class).getResultList();
    }

    /**
     * ---UPDATE--- Método para modificar empleado
     *
     * @param empleado
     * @return verdadero (modificado) o falso (no modificado)
     */
    @Transactional(rollbackFor = {PersistenceException.class, Exception.class})
    public Boolean update(Empleado empleado) {
        try {
            em.merge(empleado);
            return true;
        } catch (PersistenceException pe) {
            System.err.println("Error de persistencia: " + pe.getMessage());
        } catch (Exception ex) {
            System.err.println("Error desconocido: " + ex.getMessage());
        }
        return false;
    }

    /**
     * Método para encontrar empleado por id
     *
     * @param id
     * @return Empleado
     */
    @Transactional(rollbackFor = {PersistenceException.class, Exception.class})
    public Empleado findById(Integer id) {
        return em.find(Empleado.class, id);
    }

    public Empleado login(String username, String cod_empleado) {
        Empleado empleado = null;

        Query query = em.createNamedQuery("FROM Empleado e WHERE e.usuario = :usuario AND e.cod_empleado = :cod_empleado");

        query.setParameter("usuario", username);
        query.setParameter("cod_empleado", cod_empleado);

        List<Empleado> empleados = query.getResultList();

        if (empleados != null) {
            try {
                empleado = empleados.get(0);
                System.out.println("Empleado ingresó al sistema");
            } catch (PersistenceException pe) {
                System.err.println("Error de persistencia: " + pe.getMessage());
            } catch (Exception ex) {
                System.err.println("Error desconocido: " + ex.getMessage());
            }
        }

        return empleado;
    }
    
    public Empleado getByUserName(String user){
        Empleado e = em.createNamedQuery("Empleado.findByUserName",Empleado.class).setParameter("user", user).getResultList().get(0);
        return em.createNamedQuery("Empleado.findByUserName",Empleado.class).setParameter("user", user).getResultList().get(0);
    }
    
    /**
     * Metodo para buscar los empleados segun su disponibilidad
     * @return 
     */
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    public List<Empleado> buscarDispo(){
        
        return (List<Empleado>) em.createNamedQuery("Empleado.findByDisponible", Empleado.class).setParameter("dispo", true).getResultList();
    }
    
    @Transactional
    public void habilitarEmpleado(String action, Empleado empleado){
        if(action.equals("habilitar")){
            empleado.setEnabled(true);
        }
        em.merge(empleado);
    }
    
    @Transactional
    public void inhabilitarEmpleado(String action, Empleado empleado){
        if(action.equals("inhabilitar")){
            empleado.setEnabled(false);
        }
        em.merge(empleado);
    }
    
}
