/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolationException;
import org.proyectosfgk.entidad.Proyecto;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class ProyectoDAO {
    @PersistenceContext
    EntityManager em;
    
    /**
     * ---CREATE--- Método para crear proyecto
     *
     * @param proyecto
     * @return verdadero (insertado) o falso (no insertado)
     */
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    public Boolean crear (Proyecto proyecto){
        try{
            em.persist(proyecto);
            em.flush();
            em.clear();
            return true;
        }catch (PersistenceException pe){
            System.err.println("Error de persistencia: " + pe.getMessage());
        }catch (ConstraintViolationException cve){
            System.err.println("Error desconocido: " + cve.getConstraintViolations().toString());
        }catch (Exception e){
            System.err.println("Error desconocido: " + e.getMessage());
        }finally{
            
        }
        
        return false;
    }
    
    /**
     * ---READ--- Método para buscar todos los proyectos
     *
     * @return lista de proyectos
     */
    @Transactional
    public List<Proyecto> findAll(){
       return em.createNamedQuery("Proyecto.findAll",Proyecto.class).getResultList();
    }
    
    /**
     * ---READ BY ID--- Método para encontrar un proyecto segun su id
     *
     * @param id
     * @return proyecto encontrado
     */
    @Transactional
    public Proyecto findById(int id){
      return em.find(Proyecto.class, id);
    }
    
    /**
     * Metodo para buscar los proyectos que estan en proceso
     * @return 
     */
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    public List<Proyecto> buscarEnProceso(){        
        return (List<Proyecto>) em.createNamedQuery("Proyecto.findByEstado", Proyecto.class).setParameter("estatus", "Proceso").getResultList();
    }
    
    /**
     * ---UPDATE--- Método para editar el proyecto
     *
     * @param proyecto
     * @return verdadero (modificado) o falso (no modificado)
     */
    
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    public Boolean actualizar (Proyecto proyecto){
        try{
            em.merge(proyecto);
            em.flush();
            em.clear();
            return true;
        }catch (PersistenceException pe){
            System.err.println("Error de persistencia: " + pe.getMessage());
        }catch (Exception e){
            System.err.println("Error desconocido: " + e.getMessage());
        }
        return false;
    }
}
