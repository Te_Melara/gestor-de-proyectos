/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import org.proyectosfgk.entidad.Permiso;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author rodolfo.molinafgkss
 */
@Service
public class PermisoDAO {

    @PersistenceContext
    private EntityManager em;

    /**
     * ---CREATE--- Método para crear permiso
     *
     * @param permiso
     * @return verdadero (insertado) o falso (no insertado)
     */
    @Transactional(rollbackFor = {PersistenceException.class, Exception.class})
    public Boolean create(Permiso permiso) {
        try {
            em.persist(permiso);
            return true;
        } catch (PersistenceException pe) {
            System.err.println("Error de persistencia: " + pe.getMessage());
        } catch (Exception ex) {
            System.err.println("Error desconocido: " + ex.getMessage());
        }
        return false;
    }

    /**
     * ---READ--- Método para buscar permisos
     *
     * @return lista de permisos
     */
    @Transactional(rollbackFor = {PersistenceException.class, Exception.class})
    public List<Permiso> findAll() {
        return em.createNamedQuery("Permiso.findAll", Permiso.class).getResultList();
    }

    /**
     * ---UPDATE--- Método para editar permiso
     *
     * @param permiso
     * @return verdadero (modificado) o falso (no modificado)
     */
    @Transactional(rollbackFor = {PersistenceException.class, Exception.class})
    public Boolean update(Permiso permiso) {
        try {
            em.merge(permiso);
            return true;
        } catch (PersistenceException pe) {
            System.err.println("Error de persistencia: " + pe.getMessage());
        } catch (Exception ex) {
            System.err.println("Error desconocido: " + ex.getMessage());
        }

        return false;
    }

    /**
     * ---DELETE--- Método para eliminar un permiso
     *
     * @param permiso
     * @return verdadero (eliminado) o falso(no eliminado)
     */
    @Transactional(rollbackFor = {PersistenceException.class, Exception.class})
    public Boolean remove(Permiso permiso) {
        try {
            em.remove(em.merge(permiso));
            return true;
        } catch (PersistenceException pe) {
            System.err.println("Error de persistencia: " + pe.getMessage());
        } catch (Exception e) {
            System.err.println("Error desconocido: " + e.getMessage());
        }
        return false;
    }

    /**
     * ---READ BY ID--- Método para encontrar permiso por id
     *
     * @param id
     * @return permiso encontrado
     */
    @Transactional(rollbackFor = {PersistenceException.class, Exception.class})
    public Permiso findById(Integer id) {
        return em.find(Permiso.class, id);
    }

}
