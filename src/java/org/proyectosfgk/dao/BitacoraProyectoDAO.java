/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.dao;

import java.text.DecimalFormat;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.proyectosfgk.entidad.BitacoraProyecto;
import org.springframework.stereotype.Service;
import java.util.List;
import javax.persistence.PersistenceException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.proyectosfgk.entidad.Proyecto;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author fernand.vasquezfgkss
 */
@Service
public class BitacoraProyectoDAO {

    @PersistenceContext
    EntityManager em;

    boolean var;
    
    /**
     * ---Crear -- recibe la bitacora a crear
     * @param bt 
     * @return un booleano que sera verdadero si no sucede ningun error
     */
    @Transactional(noRollbackFor = Exception.class)
    public boolean crear(BitacoraProyecto bt) {
        try {
            em.persist(bt);
        } catch (ConstraintViolationException e) {
            em.merge(bt);
            var = false;
            System.out.println("<-----------Error---------> " + e.getConstraintViolations());
        }
        return var;
    }

    /**
     * --Update -- actualiza las bitacoras
     * @param bt la bitacora a ser modificada
     * @return un booleano que sera verdadero si no sucede ningun error
     */
    @Transactional
    public boolean Actualizar(BitacoraProyecto bt) {
        try {
            em.merge(bt);
        } catch (Exception e) {
            var = false;
            System.out.println("<------Error--------> " + e.getMessage());
        }
        return var;
    }

    /**
     * --delete -- elimina bitacoras
     * @param bt bitacora a ser eliminada
     * @return un booleano que sera verdadero si no sucede ningun error
     */
    @Transactional
    public boolean Eliminar(BitacoraProyecto bt) {
        try {
            em.remove(bt);
        } catch (Exception e) {
            var = false;
            System.out.println("<------Error--------> " + e.getMessage());
        }
        return var;
    }

    /**
     * --Lista de bitacoras por proyecto--
     * @param id id del proyecto del cual se quiere ontener las versiones
     * @return Lista de bitacoras por proyecto
     */
    @Transactional
    public List<BitacoraProyecto> getByProject(Integer id) {
        return em.createQuery("SELECT bt FROM BitacoraProyecto bt where bt.proyectoId.id = :id", BitacoraProyecto.class)
                .setParameter("id", id)
                .getResultList();
    }

    /**
     * Retorna un objeto de proyecto
     * @param id Id del proyecto que se necesita
     * @return Retorna un objeto de proyecto
     */
    @Transactional
    public Proyecto getProyect(Integer id) {
        return em.find(Proyecto.class, id);
    }

    /**
     * Version por su id
     * @param id Id de la version que se busca
     * @return Retorna un objeto de bitacora
     */
    @Transactional
    public BitacoraProyecto getById(Integer id) {
        return em.find(BitacoraProyecto.class, id);
    }

    /**
     * --Ultima version ingresada de ese proyecto--
     * @param proyecto Proyecto del cual se quiere obtener la ultima version
     * @return numero de la version minima a ingresar
     */
    @Transactional
    public Double getLastVersion(Proyecto proyecto) {
        BitacoraProyecto bt = null;
        Double vers= 0D;
        try {
            bt = em.createQuery("SELECT bt FROM BitacoraProyecto bt WHERE bt.proyectoId = :id ORDER BY bt.id DESC ", BitacoraProyecto.class)
                    .setParameter("id", proyecto)
                    .getResultList().get(0);
            vers = bt.getVersion()+0.1;
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
            vers = 1D;
        }
        return Double.parseDouble(new DecimalFormat("#.0").format(vers));
    }
}
