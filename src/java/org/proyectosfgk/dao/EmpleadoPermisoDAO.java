package org.proyectosfgk.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.proyectosfgk.entidad.Empleado;
import org.proyectosfgk.entidad.EmpleadoPermiso;
import org.proyectosfgk.entidad.Permiso;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author fernand.vasquezfgkss
 */
@Service
public class EmpleadoPermisoDAO {

    private boolean var = false;

    @PersistenceContext
    EntityManager em;

    /**
     * Metodo que devuelve un permiso asignado a un empleado
     *
     * @param id Recibe el id para buscar el permiso de un empleado especifico
     * @return Devuelve un permiso asignado a un empleado
     */
    @Transactional
    public EmpleadoPermiso getById(int id) {
        return em.find(EmpleadoPermiso.class, id);
    }

    /**
     * Metodo que devuelve Lista de permisos asignados
     *
     * @return retorna una lista d permisos asignados
     */
    @Transactional
    public List<EmpleadoPermiso> findAll() {
        return em.createNamedQuery("EmpleadoProyecto.findAll", EmpleadoPermiso.class).getResultList();
    }

    /**
     * Metodo que asigna permisos
     *
     * @param emp Objeto de permiso asinado
     * @return retorna una lista d permisos asignados
     */
    @Transactional
    public boolean create(EmpleadoPermiso emp) {
        try {
            em.persist(emp);
            var = true;
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

        return var;
    }

    /**
     * Borra permisos asignados
     *
     * @param emp Objeto de permiso asinado
     * @return Un parametro booleano que dice si se elimino correctamente el
     * dato
     */
    @Transactional
    public boolean delete(EmpleadoPermiso emp) {
        try {
            em.remove(em.merge(emp));
            var = true;
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

        return var;
    }

    /**
     * Actualiza permisos asignados
     *
     * @param emp Objeto de permiso asinado
     * @return Un parametro booleano que dice si se actualizo correctamente el
     * dato
     */
    @Transactional
    public boolean update(EmpleadoPermiso emp) {
        try {
            em.merge(emp);
            var = true;
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

        return var;
    }

    /**
     * Lista de permisos asignados a un empleado
     *
     * @param empleado Objeto empleado de el que se retornaran el listado de
     * permisos
     * @return Lista de permisos de el empleado
     */
    @Transactional
    public List<EmpleadoPermiso> getByEmployed(Empleado empleado) {
        return em.createQuery("SELECT e FROM EmpleadoPermiso e WHERE e.empleadoId = :empleado", EmpleadoPermiso.class)
                .setParameter("empleado", empleado)
                .getResultList();
    }

    /**
     * Lista de permisos disponibles para un empleado
     * @param empleado Objeto empleado de el que se retornaran el listado de permisos disponibles
     * @return Lista de permisos disponibles de el empleado
     */
    @Transactional
    public List<Permiso> dispoList(Empleado empleado) {
        List<EmpleadoPermiso> reEmp = em.createQuery("SELECT e FROM EmpleadoPermiso e WHERE e.empleadoId = :empleado", EmpleadoPermiso.class)
                .setParameter("empleado", empleado)
                .getResultList();
        List<Permiso> permisos = em.createNamedQuery("Permiso.findAll", Permiso.class).getResultList();
        for (EmpleadoPermiso e : reEmp) {
            if (permisos.contains(e.getPermisoId())) {
                permisos.remove(e.getPermisoId());
            }
        }
        return permisos;
    }

    /**
     * Lista de empleados
     * @return Lista de empleados
     */
    @Transactional
    public List<Empleado> empleados() {
        return em.createNamedQuery("Empleado.findAll", Empleado.class).getResultList();
    }

    /**
     * Busca empleado por su id
     * @param id Id a buscar en los registros de empleados
     * @return Objeto de empleados
     */
    @Transactional
    public Empleado empleados(int id) {
        return em.find(Empleado.class, id);
    }

    /**
     * Busca permiso por su id
     * @param id Id a buscar en los registros de permisos
     * @return Objeto de permisos
     */
    @Transactional
    public Permiso permiso(int id) {
        return em.find(Permiso.class, id);
    }

    /**
     * Busca empleado por su id
     * @param accion  especifica si se realizara la accion de activar o desactivar el permiso asignado del empleado
     * @param e El empleado del cual se cambiara el estado del permiso asignado
     * @param p El permiso que se activara/desactivara
     */
    @Transactional
    public void activar(String accion, Empleado e, Permiso p) {
        EmpleadoPermiso empleado = em.createNamedQuery("EmpleadoPermiso.employedPermisos", EmpleadoPermiso.class)
                .setParameter("empleado", e)
                .setParameter("permiso", p)
                .getResultList().get(0);
        if (accion.equals("activar")) {
            empleado.setEstado(true);
        } else {
            empleado.setEstado(false);
        }
    }
    
    /**
     * Crea una asignacion de un permiso a un empleado
     * @param e El empleado del cual se asignara el permiso
     * @param p El permiso que se asignara
     */
    @Transactional
    public void crear(Empleado e, Permiso p) {
        EmpleadoPermiso emp = new EmpleadoPermiso();
        emp.setEmpleadoId(e);
        emp.setEstado(true);
        emp.setPermisoId(p);
        em.persist(emp);
    }
}
