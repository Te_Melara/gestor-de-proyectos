/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import org.proyectosfgk.entidad.Rol;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author amand.rodriguezfgkss
 */
@Service
public class RolDAO {
    
    @PersistenceContext
    private EntityManager em;
    
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    public List<Rol> buscarR(){
        
        return em.createNamedQuery("Rol.findAll", Rol.class).getResultList();
    }
    
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    public Rol buscarPorId(Integer id){
        
        return em.find(Rol.class, id);
    }
    
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    public Boolean crear(Rol rol){
        
        try{
            em.persist(rol);
            return true;
            
        }catch(PersistenceException pe){
            System.err.println("Error de persistencia" + pe.getMessage());
        }catch(Exception e){
            System.err.println("Errror desconocido" + e.getMessage());
        }return false;
        
    }
    
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    public Boolean eliminar (Rol rol){
        
        try{
            rol = em.merge(rol);
            em.remove(rol);
          
            return true;
            
        }catch(PersistenceException pe){
            System.err.println("Error de persistencia" + pe.getMessage());
        }catch(Exception e){
            System.err.println("Error desconocido" + e.getMessage());
        }return false;
    }
    
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    public Boolean actualizar(Rol rol){
        
        try{
            em.merge(rol);
            return true;
        }catch(PersistenceException pe){
            System.err.println("Error de persistencia" + pe.getMessage());
        }catch(Exception e){
            System.err.println("Error desconocido" + e.getMessage());
        }return false;
    }
    
}
