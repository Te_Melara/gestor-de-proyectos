/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import org.proyectosfgk.entidad.Departamento;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author amand.rodriguezfgkss
 */
@Service
public class DepartamentoDAO {
    
    @PersistenceContext
    private EntityManager em;
    
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    
    public List<Departamento> buscarDepartamento(){
        
        return em.createNamedQuery("Departamento.findAll", Departamento.class).getResultList();
    }
    
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    public Departamento buscarPorId(Integer id){
        
        return em.find(Departamento.class, id);
    }
    
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    public Boolean crear(Departamento d){
        try{
            em.persist(d);
            return true;
        }catch(PersistenceException pe){
            System.err.println("Error de persistencia" + pe.getMessage());
        }catch(Exception e){
            System.err.println("Error desconocido" + e.getMessage());
        }return false;
    }
    
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    public Boolean eliminar (Departamento d){
        try{
            d = em.merge(d);
            em.remove(d);
            return true;
        }catch(PersistenceException pe){
            System.err.println("Error de persisitencia" + pe.getMessage());
        }catch(Exception e){
            System.err.println("Error desconocido" + e.getMessage());
        }return false;
    }
    
    @Transactional (rollbackFor = {PersistenceException.class, Exception.class})
    public Boolean actualizar(Departamento d){
        try{
            em.merge(d);
            return true;
        }catch(PersistenceException pe){
            System.err.println("Error de persistencia" + pe.getMessage());
        }catch(Exception e){
            System.err.println("Error desconocido" + e.getMessage());
        }return false;
    }
    
   }
