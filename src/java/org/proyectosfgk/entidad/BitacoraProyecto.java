/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author fernand.vasquezfgkss
 */
@Entity
@Table(name = "bitacora_proyecto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BitacoraProyecto.findAll", query = "SELECT b FROM BitacoraProyecto b")
    , @NamedQuery(name = "BitacoraProyecto.findById", query = "SELECT b FROM BitacoraProyecto b WHERE b.id = :id")
    , @NamedQuery(name = "BitacoraProyecto.findByFecha", query = "SELECT b FROM BitacoraProyecto b WHERE b.fecha = :fecha")
    , @NamedQuery(name = "BitacoraProyecto.findByVersion", query = "SELECT b FROM BitacoraProyecto b WHERE b.version = :version")
    , @NamedQuery(name = "BitacoraProyecto.findByFormato", query = "SELECT b FROM BitacoraProyecto b WHERE b.formato = :formato")
    , @NamedQuery(name = "BitacoraProyecto.findByNombre", query = "SELECT b FROM BitacoraProyecto b WHERE b.nombre = :nombre")})
public class BitacoraProyecto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Lob
    @Column(name = "archivo")
    private byte[] archivo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "version")
    private Double version;
    @Basic(optional = false)
    @NotNull
    @Column(name = "formato")
    private String formato;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "nombre")
    private String nombre;
    @JoinColumn(name = "proyecto_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Proyecto proyectoId;

    public BitacoraProyecto() {
    }

    public BitacoraProyecto(Integer id) {
        this.id = id;
    }

    public BitacoraProyecto(Integer id, Date fecha, Double version, String formato, String nombre) {
        this.id = id;
        this.fecha = fecha;
        this.version = version;
        this.formato = formato;
        this.nombre = nombre;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Proyecto getProyectoId() {
        return proyectoId;
    }

    public void setProyectoId(Proyecto proyectoId) {
        this.proyectoId = proyectoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BitacoraProyecto)) {
            return false;
        }
        BitacoraProyecto other = (BitacoraProyecto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.proyectosfgk.entidad.BitacoraProyecto[ id=" + id + " ]";
    }
    
}
