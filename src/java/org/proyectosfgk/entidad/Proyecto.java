/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 *
 * @author tania.melarafgkss
 */
@Entity
@Table(name = "proyecto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proyecto.findAll", query = "SELECT p FROM Proyecto p")
    , @NamedQuery(name = "Proyecto.findById", query = "SELECT p FROM Proyecto p WHERE p.id = :id")
    , @NamedQuery(name = "Proyecto.findByNombre", query = "SELECT p FROM Proyecto p WHERE p.nombre = :nombre")
    , @NamedQuery(name = "Proyecto.findByDescripcion", query = "SELECT p FROM Proyecto p WHERE p.descripcion = :descripcion")
    , @NamedQuery(name = "Proyecto.findByFechaInicio", query = "SELECT p FROM Proyecto p WHERE p.fechaInicio = :fechaInicio")
    , @NamedQuery(name = "Proyecto.findByFechaFin", query = "SELECT p FROM Proyecto p WHERE p.fechaFin = :fechaFin")
    , @NamedQuery(name = "Proyecto.findByTipoProyecto", query = "SELECT p FROM Proyecto p WHERE p.tipoProyecto = :tipoProyecto")
    , @NamedQuery(name = "Proyecto.findByEstado", query = "SELECT p FROM Proyecto p WHERE p.estado = :estatus")
    , @NamedQuery(name = "Proyecto.findByFormato", query = "SELECT p FROM Proyecto p WHERE p.formato = :formato")
    , @NamedQuery(name = "Proyecto.findByNombreArchivo", query = "SELECT p FROM Proyecto p WHERE p.nombreArchivo = :nombreArchivo")})
public class Proyecto implements Serializable {

    @Basic(optional = false)
    @NotNull
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.DATE)
    private Date fechaInicio;
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.DATE)
    private Date fechaFin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "tipo_proyecto")
    private String tipoProyecto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private String estado;
    @Lob
    @Column(name = "requerimientos")
    private byte[] requerimientos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "formato")
    private String formato;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nombre_archivo")
    private String nombreArchivo;
    @JoinColumn(name = "lider_proyecto", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empleado liderProyecto;
     @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyectoId")
    private List<EmpleadoProyecto> empleadoProyectoList;

    public List<EmpleadoProyecto> getEmpleadoProyectoList() {
        return empleadoProyectoList;
    }

    public void setEmpleadoProyectoList(List<EmpleadoProyecto> empleadoProyectoList) {
        this.empleadoProyectoList = empleadoProyectoList;
    }

    @Transient
    private CommonsMultipartFile archivo;

    public CommonsMultipartFile getArchivo() {
        return archivo;
    }

    public void setArchivo(CommonsMultipartFile archivo) {
        this.archivo = archivo;
    }
    
    public Proyecto() {
    }

    public Proyecto(Integer id) {
        this.id = id;
    }

    public Proyecto(Integer id, String nombre, Date fechaInicio, String tipoProyecto, String estado, String formato, String nombreArchivo) {
        this.id = id;
        this.nombre = nombre;
        this.fechaInicio = fechaInicio;
        this.tipoProyecto = tipoProyecto;
        this.estado = estado;
        this.formato = formato;
        this.nombreArchivo = nombreArchivo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getTipoProyecto() {
        return tipoProyecto;
    }

    public void setTipoProyecto(String tipoProyecto) {
        this.tipoProyecto = tipoProyecto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public byte[] getRequerimientos() {
        return requerimientos;
    }

    public void setRequerimientos(byte[] requerimientos) {
        this.requerimientos = requerimientos;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public Empleado getLiderProyecto() {
        return liderProyecto;
    }

    public void setLiderProyecto(Empleado liderProyecto) {
        this.liderProyecto = liderProyecto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proyecto)) {
            return false;
        }
        Proyecto other = (Proyecto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.proyectosfgk.entidad.Proyecto[ id=" + id + " ]";
    }
}
