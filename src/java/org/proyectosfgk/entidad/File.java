/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.entidad;

import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author fernand.vasquezfgkss
 */
public class File {
   private Integer idProyect;

    public Integer getIdProyect() {
        return idProyect;
    }

    public void setIdProyect(Integer idProyect) {
        this.idProyect = idProyect;
    }
   private String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
   private MultipartFile multipartFile;

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
