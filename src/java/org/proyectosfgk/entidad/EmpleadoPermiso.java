/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proyectosfgk.entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author rodolfo.molinafgkss
 */
@Entity
@Table(name = "empleado_permiso")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EmpleadoPermiso.findAll", query = "SELECT e FROM EmpleadoPermiso e")
    , @NamedQuery(name = "EmpleadoPermiso.findById", query = "SELECT e FROM EmpleadoPermiso e WHERE e.id = :id")
    , @NamedQuery(name = "EmpleadoPermiso.findByEstado", query = "SELECT e FROM EmpleadoPermiso e WHERE e.estado = :estado")
    , @NamedQuery(name = "EmpleadoPermiso.employedPermisos", query = "SELECT e FROM EmpleadoPermiso e WHERE e.empleadoId = :empleado AND e.permisoId = :permiso")})
public class EmpleadoPermiso implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private boolean estado;
    @JoinColumn(name = "empleado_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Empleado empleadoId;
    @JoinColumn(name = "permiso_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Permiso permisoId;

    public EmpleadoPermiso() {
    }

    public EmpleadoPermiso(Integer id) {
        this.id = id;
    }

    public EmpleadoPermiso(Integer id, boolean estado) {
        this.id = id;
        this.estado = estado;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Empleado getEmpleadoId() {
        return empleadoId;
    }

    public void setEmpleadoId(Empleado empleadoId) {
        this.empleadoId = empleadoId;
    }

    public Permiso getPermisoId() {
        return permisoId;
    }

    public void setPermisoId(Permiso permisoId) {
        this.permisoId = permisoId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EmpleadoPermiso)) {
            return false;
        }
        EmpleadoPermiso other = (EmpleadoPermiso) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.proyectosfgk.entidad.EmpleadoPermiso[ id=" + id + " ]";
    }
    
}
