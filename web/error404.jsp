<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE HTML>
<html>
<head>
<title>Error 404</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href='//fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister' rel='stylesheet' type='text/css'>
<style type="text/css">
body{
	font-family: 'Love Ya Like A Sister', cursive;
       
}
body{
        background-color: white;
}	
.wrap{
	margin:0 auto;
	width:100%;
}
.logo{
	text-align:center;
	margin-top:100px;
}
.logo img{
	width:350px;
}
.logo p{
	color:black;
	font-size:40px;
	margin-top:1px;
}	
.logo p span{
	color:lightgreen;
}	
.sub a{
        cursor: pointer;
	color:#fff;
	background:#272727;
	text-decoration:none;
	padding:10px 20px;
	font-size:13px;
	font-family: arial, serif;
	font-weight:bold;
	-webkit-border-radius:.5em;
	-moz-border-radius:.5em;
	-border-radius:.5em;
}	
.footer{
	color:black;
	position:absolute;
	right:10px;
	bottom:10px;
}	
.footer a{
	color:rgb(114, 173, 38);
}	
@media screen and (max-width: 1024px){
	.logo img {
		width: 330px;
	}	
	.logo {
      margin-top: 120px;
	}
}
@media screen and (max-width: 991px){
	.logo p {
		font-size: 37px;
	}
	.logo img {
		width: 315px;
	}
}
@media screen and (max-width: 900px){
	.logo img {
		width: 320px;
	}
}
@media screen and (max-width: 800px){
	.logo img {
		width: 300px;
	}
	.logo p {
		font-size: 35px;
	}
	.logo {
		margin-top: 140px;
	}
}
@media screen and (max-width: 768px){
	.logo img {
		width: 274px;
	}
}
@media screen and (max-width: 736px){
	.logo img {
		width: 250px;
	}
	.logo {
		margin-top: 160px;
	}
}
@media screen and (max-width: 667px){
	.logo {
		margin-top: 150px;
	}
	.logo img {
		width: 230px;
	}
}
@media screen and (max-width: 640px){
	.logo p {
		font-size: 33px;
	}
}
@media screen and (max-width: 600px){
	.logo p {
		font-size: 31px;
	}
	.sub a {
     padding: 8px 17px;
    	font-size: 12px;
	}
	.logo img {
		width: 208px;
	}
}
@media screen and (max-width: 568px){
	.logo p {
		font-size: 29px;
	}
	.logo {
		margin-top: 185px;
	}
	.footer {
      font-size: 15px;
	}
}
@media screen and (max-width: 480px){}
@media screen and (max-width: 414px){ 
	.logo img {
		width: 190px;
	}
	.footer {
		font-size: 20px;
		padding: 15px;
		text-align: center;
		line-height: 1.8;
	}
	.logo p {
		font-size: 27px;
	}
}
@media screen and (max-width: 384px){
	.logo img {
		width: 180px;
	}
}
@media screen and (max-width: 375px){}
@media screen and (max-width: 320px){
	.logo p {
		font-size: 25px;
	}
	.logo img {
		width: 172px;
	}
}
</style>
</head>

<spring:url value="/index.htm" var="index"/>
<body>
 <div class="wrap">
	<div class="logo">
			<p>Página no encontrada</p>
                        
                        <p>La página que buscas no existe o ha ocurrido un error inesperado.
                        Vuelve atrás, o utiliza el boton de regresar al inicio</p>
<!--                        <img src="https://i.imgur.com/JL1aW25.gif" alt="W3Schools.com">-->
			<img src="https://cdn.dribbble.com/users/547471/screenshots/3063720/not_found.gif" alt="W3Schools.com">
			<div class="sub">
			  <p><a onclick="history.back(-1)">Atras</a></p>
                          
                          
			</div>
	</div>
 </div>	
	
	
	<div class="footer">
	<b>&copy Derechos reservados CDS FGK-2019 | Design by<a>GRUPO #3</a></b>
	</div>
	
</body>