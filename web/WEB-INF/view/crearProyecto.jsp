<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="layout/header.jsp"></jsp:include>
<spring:url value="/proyectos/crearProyecto.htm" var="create"/>
<div id="page-wrapper">        
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12" align="center">
                <h1 class="page-header">CREAR PROYECO</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <div class="container">
        <div class="row" >
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">   
                                <c:if test="${m eq 'error'}">
                                    <div class="alert alert-danger fade in">
                                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                                        <strong>Error!</strong> El archivo no cumple con el formato indicado(.doc,.docx,.txt,.pdf)
                                    </div>
                                </c:if>
                                <form:form modelAttribute="proyecto" action="${create}" method="POST" enctype="multipart/form-data">
                                    <form:hidden path="estado" value="Proceso"/>
                                    <div class="form-group row">
                                        <label class="col-lg-2">Nombre:</label>
                                        <div class="col-lg-8">
                                            <form:input id="nombre" cssClass="form-control" path="nombre" required="required"/>                                    
                                        </div>                                    
                                    </div>                               
                                    <div class="form-group row">
                                        <label class="col-lg-2">Fecha de inicio:</label>
                                        <div class="col-lg-3">
                                            <input id="fechaInicio" name="fechaI" class="form-control" required="true"/>
                                        </div>            
                                        <label class="col-lg-2">Fecha de fin:</label>
                                        <div class="col-lg-3">
                                            <input id="fechaFin" name="fechaF" class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2">Tipo:</label>
                                        <div class="col-lg-3">
                                            <form:select path="tipoProyecto" items="${tiposMap}" cssClass="form-control" required="required"/>
                                        </div>
                                        <label class="col-lg-2">Líder:</label>
                                        <div class="col-lg-3">
                                            <form:select path="liderProyecto.id" items="${lideresMap}" cssClass="form-control" required="required"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2">Descripción:</label>
                                        <div class="col-lg-8">
                                            <form:textarea id="descripcion" cssClass="form-control" rows="6" path="descripcion" maxlength="250" />                                            
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2">Requerimientos:</label>
                                        <div class="col-lg-8">
                                            <form:input type="file" path="archivo"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-2">
                                        </div>
                                        <div class="col-lg-6">
                                            <!--<button type="submit" class="btn btn-success">Create</button>-->
                                            <c:if test="${not empty Crear and Crear.estado}">
                                                <input type="submit" value="Crear" id="crear" class="btn btn-success" />
                                                <button type="reset" class="btn btn-primary">Reset</button>
                                            </c:if>
                                        </div>
                                    </div>
                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#page-wrapper -->
<jsp:include page="layout/footer.jsp"></jsp:include>
<!--</body>
</html>-->
