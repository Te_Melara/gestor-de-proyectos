<%-- 
    Document   : modificarDepartamento
    Created on : 12-21-2018, 09:31:08 AM
    Author     : amand.rodriguezfgkss
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="layout/header.jsp"></jsp:include>
<spring:url value="/departamento/modificarDepartamento.htm" var="update"/>

    <div id="page-wrapper">              
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12" align="center">
                    <h1 class="page-header">EDITAR DEPARTAMENTO</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        <div class="container">
            <div class="row" >
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form action="${update}" method="Post">
                                    <div class="form-group row">
                                        <label class="col-lg-2" hidden>Id:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" type="hidden" name="id" value="${requestScope.depa.id}">                                    
                                        </div>                                    
                                    </div>                                        
                                    <div class="form-group row">
                                        <label class="col-lg-2" >Nombre:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" type="text" name="nombre" value="${requestScope.depa.nombre}">                                    
                                        </div>                                    
                                    </div>                                        
                                    <div class="form-group row">
                                        <div class="col-lg-2">

                                        </div>
                                        <div class="col-lg-6">
                                            <button type="submit" class="btn btn-success">Guardar</button>                                            
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#page-wrapper -->

<jsp:include page="layout/footer.jsp"></jsp:include>
