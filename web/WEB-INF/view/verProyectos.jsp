<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="layout/header.jsp"></jsp:include>
<spring:url value="/proyectos/finalizarProyectos.htm" var="end"/>
<spring:url value="/proyectos/modificarProyecto.htm" var="update"/>
<spring:url value="/bitacora/ver.htm" var="bita"/>
<spring:url value="/proyectos/download.htm" var="des"/>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12" align="center">
            <h1 class="page-header">Lista de Proyectos</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table width="100%" class="table table-striped table-bordered table-hover" id="myTable">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Fecha de Inicio</th>
                                <th>Fecha de Finalizacion</th>
                                <th>Tipo de Proyecto</th>
                                <th>Estado</th>
                                <th>Lider</th>
                                <th>Requerimientos</th>
                                <th>Editar</th>
                                <th>Bitacora</th>
                                <th>Finalizar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${proyectos}" var="dt">
                                <c:set var="req" value="${dt.nombreArchivo}"/>
                                <tr>
                                    <td>${dt.id}</td>
                                    <td>${dt.nombre}</td>
                                    <td>${dt.descripcion}</td>
                                    <td><fmt:formatDate value="${dt.fechaInicio}" pattern="dd-MM-yyyy"/></td>
                                    <td><fmt:formatDate value="${dt.fechaFin}" pattern="dd-MM-yyyy"/></td>
                                    <td>${dt.tipoProyecto}</td>
                                    <td>${dt.estado}</td>
                                    <td>${dt.liderProyecto.nombre} ${dt.liderProyecto.apellido}</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${not empty req}">
                                                <a class="btn btn-md btn-primary <c:if test="${empty Exportar or !Exportar.estado}"><c:out value="disabled"/></c:if>" href="${des}?id=${dt.id}">Requerimientos</a>
                                            </c:when>
                                            <c:otherwise>
                                                <span>Sin requerimientos</span>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                        <td>
                                            <a href="${update}?id=${dt.id}" 
                                           class="btn btn-md btn-primary <c:if test="${empty Modificar or !Modificar.estado or dt.estado eq 'Finalizado'}"><c:out value="disabled"/></c:if>">Editar</a>
                                        </td>
                                        <td><a class="btn btn-md btn-primary" href="${bita}?id=${dt.id}">Bitacora</a></td>
                                    <td><a href="${end}?id=${dt.id}" class="btn btn-md btn-danger <c:if test="${dt.estado eq 'Finalizado'}"><c:out value="disabled"/></c:if>">Finalizar</a></td>
                                    </tr>                   
                            </c:forEach>
                        </tbody> 
                    </table>
                    <!-- /.table-responsive -->                        
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<jsp:include page="layout/footer.jsp"></jsp:include>
