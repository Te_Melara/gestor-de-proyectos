<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="layout/header.jsp"></jsp:include>
    <spring:url value="/proyectos/modificarProyecto.htm" var="update"/>

    <div id="page-wrapper">              
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12" align="center">
                    <h1 class="page-header">EDITAR PROYECTO</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        <div class="container">
            <div class="row" >
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <c:if test="${(not empty message) and (not (message eq null))}">
                                    <div class="alert alert-danger fade in">
                                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                                        <strong>Error!</strong> El archivo no cumple con el formato indicado(.doc,.docx,.txt,.pdf)
                                    </div>
                                </c:if>
                                <form:form modelAttribute="proyecto" action="${update}" method="Post" enctype="multipart/form-data">
                                    <div class="form-group row">
                                        <label class="col-lg-2" hidden>Id:</label>
                                        <div class="col-lg-8">
                                            <form:hidden path="id"/>                                    
                                        </div>                                    
                                    </div>                                        
                                    <div class="form-group row">
                                        <label class="col-lg-2" >Nombre:</label>
                                        <div class="col-lg-8">                                        
                                            <form:input cssClass="form-control" path="nombre"/>
                                        </div>                                    
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2" >Descripcion:</label>
                                        <div class="col-lg-8">
                                            <form:textarea cssClass="form-control" rows="4" path="descripcion" maxlength="250" />
                                        </div>                                    
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2" >Fecha Inicio:</label>
                                        <div class="col-lg-8">
                                            <label class="form-control"><fmt:formatDate value="${proyecto.fechaInicio}" pattern="dd/MM/yyyy"/></label>                                       
                                        </div>                                    
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2" >Fecha Fin:</label>
                                        <div class="col-lg-8">
                                            <input type="date" class="form-control" name="fechaF" value="<fmt:formatDate value="${proyecto.fechaFin}" pattern="yyyy-MM-dd"/>"/>                                   
                                        </div>                                    
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2" >Tipo de proyecto:</label>
                                        <div class="col-lg-8">                                      
                                            <form:input cssClass="form-control" path="tipoProyecto"/>                                    
                                        </div>                                    
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2" >Estado del proyecto:</label>
                                        <div class="col-lg-8">
                                            <label class="form-control">${proyecto.estado}</label>                                                                        
                                        </div>                                    
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2" >Lider del proyecto:</label>
                                        <div class="col-lg-8">                      
                                            <label class="form-control">${proyecto.liderProyecto.nombre} ${proyecto.liderProyecto.apellido}</label>                                   
                                        </div>                                    
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2" >Requerimientos:</label>
                                        <div class="col-lg-8">
                                            <form:input type="file" cssClass="form-control" path="archivo"/>                                                                         
                                        </div>                                    
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-2">
                                        </div>
                                        <div class="col-lg-6">
                                            <button type="submit" class="btn btn-success">Modificar</button>                                            
                                        </div>
                                    </div>
                                </form:form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#page-wrapper -->
<jsp:include page="layout/footer.jsp"></jsp:include>