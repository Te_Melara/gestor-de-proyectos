<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="layout/header.jsp"></jsp:include>
<spring:url value="/departamento/crearDepartamento.htm" var="d"/>
<div id="page-wrapper">        
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12" align="center">
                <h1 class="page-header">CREAR DEPARTAMENTO</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <div class="container">
        <div class="row" >
            <div class="col-lg-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form action="${d}" method="post">
                                    <div class="form-group row">
                                        <label class="col-lg-2">Nombre:</label>
                                        <div class="col-lg-8">
                                            <input class="form-control" name="nombre" value="${dep.nombre}" required="true">                                    
                                        </div>                                    
                                    </div>
                                 
                                    <div class="form-group row">
                                        <div class="col-lg-2">

                                        </div>
                                        <div class="col-lg-6">
                                            <button type="submit" class="btn btn-success" >Ingresar</button>
                                            <button type="reset" class="btn btn-primary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#page-wrapper -->

<jsp:include page="layout/footer.jsp"></jsp:include>

<!--</body>
</html>-->
