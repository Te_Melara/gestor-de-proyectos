<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="layout/header.jsp"></jsp:include>
<c:url value="/bitacora/guardar.htm" var="save"/>
<c:url value="/bitacora/descargar.htm" var="des"/>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1>Crear nueva version</h1>
        </div>        
    </div>
    <!-- /.container-fluid -->
    <div class="container">
        <div class="row" >
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <c:if test="${message eq 'error'}">
                                    <div class="alert alert-danger fade in">
                                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                                        <strong>Error!</strong> El archivo no cumple con el formato indicado(.rar,.zip,.tar.gz,.7z)
                                    </div>
                                </c:if>
                                <c:if test="${message eq 'correcto'}">
                                    <div class="alert alert-success fade in">
                                        <a href="#" class="close" data-dismiss="alert">&times;</a>
                                        <strong>Correcto!</strong> La nueva version fue creada.
                                    </div>
                                </c:if>
                                <form:form modelAttribute="file" action="${save}" method="Post" enctype="multipart/form-data">
                                    <div class="form-group row">
                                        <form:hidden path="idProyect" value="${proyecto.id}"/>
                                        <label class="col-lg-2" for="file">Archivo de versión</label>
                                        <div class="col-lg-3">
                                            <form:input path="multipartFile" type="file" required="required"/>
                                        </div>  
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-lg-2">Version entera</label>
                                        <div class="col-lg-4">
                                            <form:input path="version" type="number" step="0.1" min="${v}" value="${v}" required="required"/>                                    
                                        </div>                                    
                                        <c:if test="${not empty Crear}">
                                            <c:if test="${Crear.empleadoId.rolId.id == 1 or Crear.estado}">
                                                <div class="form-group row">
                                                    <div class="col-lg-2">

                                                    </div>
                                                    <div class="col-lg-6">
                                                        <c:if test="${(not empty Crear or Crear.estado) and proyecto.estado eq 'Proceso'}">
                                                            <input type="submit" value="Crear" id="crear" class="btn btn-success" />
                                                            <button type="reset" class="btn btn-primary">Reset</button>
                                                        </c:if>
                                                    </div>
                                                </div>
                                            </c:if>
                                        </c:if>
                                    </form:form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <h1>Bitacora del proyecto ${proyecto.nombre}</h1>
            </div>        
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="myTable">
                            <thead>
                            <th>Fecha</th>
                            <th>Nombre Proyecto</th>
                            <th>Version</th>
                            <th>Nombre archivo</th>
                                <c:if test="${not empty Actualizar or not empty Eliminar}">
                                    <c:if test="${Actualizar.estado or Eliminar.estado}">
                                    <th>Acciones</th>
                                    </c:if>
                                </c:if>
                            </thead>
                            <tbody>
                                <c:forEach items="${versiones}" var="bt">
                                    <tr class="odd gradeX">
                                        <td>${bt.fecha}</td>
                                        <td>${bt.proyectoId.nombre}</td>
                                        <td>${bt.version}</td>
                                        <td>${bt.nombre}</td>
                                        <td>
                                            <a class="btn btn-md btn-success <c:if test="${empty Exportar or !Exportar.estado}"><c:out value="disabled"/></c:if>" href="${des}?id=${bt.id}">Descargar</a>
                                            </td>
                                        </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <!-- /.table-responsive -->                        
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>    
    </div>

    <jsp:include page="layout/footer.jsp"></jsp:include>
