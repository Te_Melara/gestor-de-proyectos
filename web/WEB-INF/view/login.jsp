<%-- 
    Document   : login
    Created on : Jan 2, 2019, 11:14:42 AM
    Author     : rodolfo.molinafgkss
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Gestión-Proyectos</title>

        <!-- Bootstrap Core CSS -->                        
        <link href="<spring:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet"/>

        <!-- MetisMenu CSS -->
        <link href="<spring:url value="/resources/vendor/metisMenu/metisMenu.min.css"/>" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<spring:url value="/resources/dist/css/sb-admin-2.css"/>" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="<spring:url value="/resources/vendor/morrisjs/morris.css"/>" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<spring:url value="/resources/vendor/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css">

    </head>

    <body>        
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Please Sign In</h3>
                        </div>
                        <div class="panel-body">
                            <c:if test="${param.error != null}">
                                <span style="color: red">Error de credenciales</span>
                            </c:if>
                            <form action="<spring:url value="j_spring_security_check" />" method="POST">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="User" name="username" type="text" autofocus>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input id="remember" name="remember_me" type="checkbox" value="true">Remember Me
                                        </label>
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <input class="btn btn-lg btn-success btn-block" type="submit" value="Login"/>
                                    <input class="btn btn-lg btn-primary btn-block" type="reset" value="Reset"/>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="<spring:url value="/resources/vendor/jquery/jquery.min.js"/>"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<spring:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<spring:url value="/resources/vendor/metisMenu/metisMenu.min.js"/>"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<spring:url value="/resources/dist/js/sb-admin-2.js"/>"></script>
    </body>
</html>
