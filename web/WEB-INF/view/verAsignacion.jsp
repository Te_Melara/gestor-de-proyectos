<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="layout/header.jsp"></jsp:include>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12" align="center">
                <h1 class="page-header">Lista de Asignación de Proyectos</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="myTable">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Proyecto</th>
                                    <th>Empleado</th>                                                                        
                                </tr>
                            </thead>
                        <tbody>
                        <c:forEach items="${listaAsig}" var="empr">
                            <tr>
                                <td>${empr.id}</td>
                                <td>${empr.proyectoId.nombre}</td>
                                <td>${empr.empleadoId.nombre} ${empr.empleadoId.apellido}</td>                                            
                            </tr>                   
                        </c:forEach>
                        </tbody> 
                    </table>
                    <!-- /.table-responsive -->                        
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
</div>
<jsp:include page="layout/footer.jsp"></jsp:include>