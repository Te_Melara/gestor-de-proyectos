<%-- 
    Document   : modificarEmpleado
    Created on : Jan 9, 2019, 9:07:16 AM
    Author     : rodolfo.molinafgkss
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="layout/header.jsp"></jsp:include>
    
    <div id="page-wrapper">              
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12" align="center">
                    <h1 class="page-header">EDITAR EMPLEADO</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
        <div class="container">
            <div class="row" >
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div class="row">

                                <div class="col-lg-12">
                                    <form action="${create}" method="POST">
                                        <div class="form-group row">
                                            <label class="col-lg-1">Nombres:</label>
                                            <div class="col-lg-5">
                                                <input class="form-control" name="nombre" value="${requestScope.empleado.nombre}">                                    
                                            </div>

                                            <label class="col-lg-1">Apellidos:</label>
                                            <div class="col-lg-5">
                                                <input class="form-control" name="apellido" value="${requestScope.empleado.apellido}">                                    
                                            </div>
                                        </div>
                                        <div class="form-group row">

                                            <label class="col-lg-1">Depto:</label>
                                            <div class="col-lg-2">
                                                <select class="form-control" name="departamentoId.id">
                                                <c:forEach items="${listaDepartamentos}" var="departamento">
                                                    <option value="${departamento.id}"<c:if test="${empleado.departamentoId.id == departamento.id}"><c:out value="selected"></c:out></c:if>>${departamento.nombre}</option>                                                    
                                                </c:forEach>

                                            </select>
                                        </div>

                                        <label class="col-lg-1">Rol:</label>
                                        <div class="col-lg-2">
                                            <select class="form-control" name="rolId.id">
                                                <c:forEach items="${listaRoles}" var="rol">
                                                    <option value="${rol.id}"<c:if test="${empleado.rolId.id == rol.id}"><c:out value="selected"></c:out></c:if>>${rol.nombre}</option>
                                                </c:forEach>
                                            </select>
                                        </div>

                                        <label class="col-lg-1">Usuario:</label>
                                        <div class="col-lg-3">
                                            <input class="form-control" type="text" name="usuario" value="${requestScope.empleado.usuario}">
                                        </div>

                                        <label class="col-lg-1">Código:</label>
                                        <div class="col-lg-1">
                                            <input class="form-control" type="text" name="codEmpleado">
                                        </div>

                                    </div>
                                    <div>
                                        <div class="col-lg-10"></div>
                                        <div class="col-lg-2">
                                            <button type="submit" class="btn btn-success">Guardar</button>
                                            <button type="reset" class="btn btn-primary">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /#page-wrapper -->
<jsp:include page="layout/footer.jsp"></jsp:include>
