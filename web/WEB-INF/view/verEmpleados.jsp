
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<jsp:include page="layout/header.jsp"></jsp:include>
<spring:url value="/empleados/modificarEmpleado.htm" var="update"/>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12" align="center">
            <h1 class="page-header">Lista de Empleados</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">                        
                    <table width="100%" class="table table-striped table-bordered table-hover" id="myTable">
                        <thead>                            
                                <th>COD</th>
                                <th>Nombre</th>                                    
                                <th>Apellido</th>
                                <th>Disponible</th>
                                <th>Usuario</th>                                                                
                                <th>Departamento</th>
                                <th>Rol</th>
                                <th>Editar</th>
                                <th>Permisos</th>
                                <th>Estado</th>
                            
                        </thead>
                        <tbody>
                            <c:forEach items="${listaEmpleados}" var="empleado">
                                <tr>
                                    <td>${empleado.id}</td>
                                    <td>${empleado.nombre}</td>
                                    <td>${empleado.apellido}</td>                                    
                                        <c:choose>
                                            <c:when test="${empleado.disponible}">
                                                <td>Si</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td>No</td>
                                            </c:otherwise>
                                        </c:choose>                                                                        
                                    <td>${empleado.usuario}</td>                                                                                                      
                                    <td>${empleado.departamentoId.nombre}</td>
                                    <td>${empleado.rolId.nombre}</td>
                                    <td><a href="${update}?id=${empleado.id}" class="btn btn-md btn-primary ">Editar</a></td>
                                    <td>
                                        <a href="<spring:url value="/empleadoPermisos/listaByEmploy.htm?id=${empleado.id}"/>" class="btn btn-sm btn-success">Ver permisos</a>
                                    </td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${empleado.enabled}">
                                                <a class="btn btn-sm btn-danger" href="<c:url value="/empleados/inhabilitar.htm"/>?id=${empleado.id}">Inhabilitar</a>
                                                
                                            </c:when>
                                            <c:otherwise>
                                                <a class="btn btn-sm btn-danger" href="<c:url value="/empleados/habilitar.htm"/>?id=${empleado.id}">Habilitar</a>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>                   
                    <!-- /.table-responsive -->                        
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

</div>

<jsp:include page="layout/footer.jsp"></jsp:include>
