<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="layout/header.jsp"></jsp:include>

    <div id="page-wrapper">      
    <spring:url value="/asignacion/crearAsignacion.htm" var="create"/>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12" align="center">
                <h1 class="page-header">Asignación de proyecto</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    <div class="container">
        <div class="row" >
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form action="${create}" method="POST">
                                    <div class="form-group row">
                                        <label class="col-lg-1">Empleado:</label>
                                        <div class="col-lg-3">
                                            <select class="form-control" name="empleadoId">
                                                <c:forEach items="${lista}" var="li">
                                                    <option value="${li.id}">${li.nombre} ${li.apellido}</option>                                   
                                                </c:forEach>
                                            </select>
                                        </div>  
                                        <label class="col-lg-1">Proyecto:</label>
                                        <div class="col-lg-3">
                                            <select class="form-control" name="proyectoId">
                                                <c:forEach items="${listado}" var="lista">
                                                    <option value="${lista.id}">${lista.nombre}</option>                                   
                                                </c:forEach>
                                            </select>
                                        </div>  
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-4">
                                        </div>
                                        <div class="col-lg-2">
                                            
                                            <c:choose>
                                                <c:when test="${listado.size() <= 0}">
                                                    <input type="submit" class="btn btn-success" value="Asignar" disabled/>
                                                </c:when>
                                                <c:otherwise>
                                                    <input type="submit" class="btn btn-success" value="Asignar"/>
                                                </c:otherwise>
                                            </c:choose>
                                           
                                            <!--<button type="submit" class="btn btn-success">Crear</button>-->
                                        </div>
                                    </div>
                                </form>                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>                                    
<!-- /#page-wrapper -->


<jsp:include page="layout/footer.jsp"></jsp:include>