<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<!-- jQuery -->
<script src="<spring:url value="/resources/vendor/jquery/jquery.min.js"/>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<spring:url value="/resources/vendor/bootstrap/js/bootstrap.min.js"/>"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<spring:url value="/resources/vendor/metisMenu/metisMenu.min.js"/>"></script>

<!-- Morris Charts JavaScript -->
<script src="<spring:url value="/resources/vendor/raphael/raphael.min.js"/>"></script>
<script src="<spring:url value="/resources/vendor/morrisjs/morris.min.js"/>"></script>
<script src="<spring:url value="/resources/data/morris-data.js"/>"></script>

<!-- Custom Theme JavaScript -->
<script src="<spring:url value="/resources/dist/js/sb-admin-2.js"/>"></script>

<!-- Datatable JavaScript -->
<script src="<spring:url value="/resources/vendor/bootstrap/js/jquery.dataTables.min.js"/>"></script>

<script src="<spring:url value="https://ajax.googleapis.com/ajax/libs/d3js/5.7.0/d3.min.js"/>"></script>

<script src="<spring:url value="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"/>"></script>

</body>
<script>
$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>

<script>
    $(function () {
        $("#fechaInicio").datepicker({
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
            onSelect: function (selectdate) {
                var dt = new Date(selectdate);
                dt.setDate(dt.getDate() + 1);
                $("#fechaFin").datepicker("option", "minDate", dt);
            }
        });

        $("#fechaFin").datepicker({
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
            onSelect: function (selectdate) {
                var dt = new Date(selectdate);
                dt.setDate(dt.getDate() - 1);
                $("#fechaInicio").datepicker("option", "maxDate", dt);
            }
        });

    });
</script>
</html>
