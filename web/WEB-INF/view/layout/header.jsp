<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Gestion-Proyectos FGK - SB Admin 2</title>

        <!-- Bootstrap Core CSS -->                        
        <link href="<spring:url value="/resources/vendor/bootstrap/css/bootstrap.min.css"/>" rel="stylesheet"/>

        <!-- MetisMenu CSS -->
        <link href="<spring:url value="/resources/vendor/metisMenu/metisMenu.min.css"/>" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<spring:url value="/resources/dist/css/sb-admin-2.css"/>" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="<spring:url value="/resources/vendor/morrisjs/morris.css"/>" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<spring:url value="/resources/vendor/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet" type="text/css">

        <!-- Datatable CSS -->
        <link href="<spring:url value="/resources/vendor/bootstrap/css/jquery.dataTables.min.css"/>" rel="stylesheet" type="text/css">   
        
        <link href="<spring:url value="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css"/>" rel="stylesheet" type="text/css">                            


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<spring:url value="/index.htm" />">Gestion de Proyectos FGK - SB Admin 2</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-messages">                        
                            <li>
                                <a class="text-center" href="#">
                                    <strong>Read All Messages</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-messages -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-tasks">                        
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>See All Tasks</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-tasks -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">                        
                            <li>
                                <a class="text-center" href="#">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-alerts -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <sec:authentication property="principal.username" var="user"/>
                            <li><a href="#"><i class="fa fa-user fa-fw"></i> ${user} </a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="<spring:url value="/salir" />"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" style="height: 34px">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                <!-- /input-group -->
                            </li>
                            <li>
                                <a href="<spring:url value="/index.htm" />"><i class="fa fa-dashboard fa-fw"></i> Home</a>
                            </li>

                            <li>
                                <a><i class="fa fa-pencil-square-o fa-fw"></i> Proyectos<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<spring:url value="/proyectos/crearProyecto.htm" />"> Crear proyecto</a>
                                    </li>
                                    <li>
                                        <a href="<spring:url value="/proyectos/verProyectos.htm" />"> Ver proyectos</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <sec:authorize access="hasRole('Administrador')">
                            <li>
                                <a><i class="fa fa-lock"></i> Permisos<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<spring:url value="/permisos/crearPermiso.htm" />"> Nuevo permiso</a>
                                    </li>
                                    <li>
                                        <a href="<spring:url value="/permisos/verPermisos.htm" />"> Ver permisos</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            
                            <li>
                                
                                <a><i class="fa fa-users"></i> Empleados<span class="fa arrow"></span></a>
                                
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<spring:url value="/empleados/crearEmpleado.htm" />"> Registrar empleado</a>
                                    </li>
                                    <li>
                                        <a href="<spring:url value="/empleados/verEmpleados.htm" />"> Ver empleados</a>
                                    </li>
                                </ul>
                                
                                <!-- /.nav-second-level -->
                            </li>
                            

                            <li>
                                <a><i class="fa fa-building-o"></i> Departamentos:<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<spring:url value="/departamento/crearDepartamento.htm" />"> Nuevo departamento</a>
                                    </li>
                                    <li>
                                        <a href="<spring:url value="/departamento/verDepartamento.htm" />"> Ver departamentos</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            
                            <li>
                                <a><i class="fa fa-flag-o"></i> Roles<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<spring:url value="/rol/crearRol.htm" />"> Nuevo rol</a>
                                    </li>
                                    <li>
                                        <a href="<spring:url value="/rol/verRoles.htm" />"> Ver roles</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            </sec:authorize>
                            <li>
                                <a href=""><i class="fa fa-files-o"></i> Asignación Proyectos<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<spring:url value="/asignacion/crearAsignacion.htm" />"> Asignar proyecto</a>
                                    </li>
                                    <li>
                                        <a href="<spring:url value="/asignacion/verAsignacion.htm" />"> Ver Asignaciones</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>                            
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>            
            <!-- /#page-wrapper -->
        </div>
        <!-- /#wrapper -->



