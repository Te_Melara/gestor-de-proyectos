<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<jsp:include page="layout/header.jsp"></jsp:include>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12" align="center">
                <h1 class="page-header">Permisos del empleado ${empleado.nombre}</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <c:choose>
                        <c:when test="${empty disponibles}">
                            <span class="center">Se han asignado todos los permisos</span>
                        </c:when>
                        <c:otherwise>
                            <form action="<c:url value="/empleadoPermisos/asignar.htm"/>?idEm=${empleado.id}" method="POST">
                                <div class="form-group">
                                    <label>Asignar permiso</label>
                                    <select name="idPer" class="form-control">
                                        <c:forEach items="${disponibles}" var="dis">
                                            <option value="${dis.id}">${dis.nombre}</option>
                                        </c:forEach> 
                                    </select>
                                </div>
                                <p>
                                    <button class="btn btn-warning" type="submit">Añadir permiso</button>
                                </p>
                            </form>
                        </c:otherwise>
                    </c:choose>

                    <div class="row">
                        <div class="col-lg-12" align="center">
                            <h1 class="page-header">Permisos asignados</h1>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <table width="100%" class="table table-striped table-bordered table-hover" id="myTable">
                        <thead>
                        <th>Permiso</th>
                        <th>Acciones</th>
                        </thead>
                        <tbody>
                            <c:forEach items="${lista}" var="per">
                                <tr class="even gradeC">
                                    <td>${per.permisoId.nombre}</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${per.estado}">
                                                <a class="btn btn-md btn-primary" href="<c:url value="/empleadoPermisos/desactivar.htm"/>?idEm=${empleado.id}&idPer=${per.permisoId.id}">Desactivar</a>
                                            </c:when>
                                            <c:otherwise>
                                                <a class="btn btn-md btn-primary" href="<c:url value="/empleadoPermisos/activar.htm"/>?idEm=${empleado.id}&idPer=${per.permisoId.id}">Activar</a>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> 
    </div>     
</div>

<jsp:include page="layout/footer.jsp"></jsp:include>
