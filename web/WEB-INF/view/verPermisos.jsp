<%-- 
    Document   : viewPermiso
    Created on : Dec 17, 2018, 3:27:23 PM
    Author     : rodolfo.molinafgkss
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<jsp:include page="layout/header.jsp"></jsp:include>
<spring:url value="/permisos/eliminarPermiso.htm" var="delete"/>
<spring:url value="/permisos/modificarPermiso.htm" var="update"/>

    <div id="page-wrapper">
    
        <div class="row">
            <div class="col-lg-12" align="center">
                <h1 class="page-header">Lista de Permisos</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-body">                        
                        <table width="100%" class="table table-striped table-bordered table-hover" id="myTable">
                            <thead>
                                <tr>
                                    <th>COD</th>
                                    <th>Nombre</th>                                    
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${listaPermisos}" var="permiso">
                                <tr>
                                    <td>${permiso.id}</td>
                                    <td>${permiso.nombre}</td>
                                    <td><a href="${update}?id=${permiso.id}" class="btn btn-md btn-primary">Editar</a></td>
                                    <td><a href="${delete}?id=${permiso.id}" class="btn btn-md btn-danger">Eliminar</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>                   
                    <!-- /.table-responsive -->                        
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

</div>

<jsp:include page="layout/footer.jsp"></jsp:include>
